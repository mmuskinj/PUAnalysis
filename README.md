# PUAnalysis

Pileup Analysis Algorithms

## Contact Details

| Contact Person | Contact E-mail        |
|----------------|-----------------------|
| Miha Muskinja  | miha.muskinja@cern.ch |

### CMake configuration

The repository comes with its own project configuration file
([CMakeLists.txt](CMakeLists.txt)). It sets up the build of all of the
code of the repository against AnalysisBase 21.2.82

To build the release project please follow the instructions below.

```
mkdir PUAnalysis; cd PUAnalysis
lsetup git
git clone ssh://git@gitlab.cern.ch:7999/mmuskinj/PUAnalysis.git
mkdir build run
cd build/
asetup asetup AnalysisBase,21.2.82
cmake ../PUAnalysis/
make -j[number of cores] (something like 'make -j8' is usually much faster than just 'make')
source x86_64[...]/setup.sh (folder name depends on the platform)
cd ../
```

### Pileup configuration

Appropriate 'NTUP_PILEUP' root files need to be copied to PUAnalysisAlgorithms/data and named in the following way:
- MC16a : background_mc16a.root
- MC16d : background_mc16d.root
- MC16e : background_mc16e.root

### Running a job

To run a job, execute for eample the following from the run folder:

```
AnalysisJob_eljob.py -i <inputFile> -t MC16e --input-type folder
```
