/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja

#ifndef PILEUP_JET_EXTRACTION_ALG_H
#define PILEUP_JET_EXTRACTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SelectionHelpers/ISelectionAccessor.h>

#include <xAODJet/JetContainer.h>


class PUJetExtractionAlg final : public EL::AnaAlgorithm {
    /// \brief the standard constructor
  public:
    PUJetExtractionAlg(const std::string &name, ISvcLocator *pSvcLocator);

  public:
    StatusCode initialize() override;

  public:
    StatusCode execute() override;

  private:
    /// \brief configurables
    double m_minJetPt;

  private:
    /// \brief various accessors
    std::unique_ptr<CP::ISelectionAccessor> m_jvt_selectionAccessor;
    std::unique_ptr<CP::ISelectionAccessor> m_fjvt_selectionAccessor;
    std::unique_ptr<const SG::AuxElement::Accessor<int> > m_PartonTruthLabelIDAcc;
    std::unique_ptr<const SG::AuxElement::Accessor<int> > m_GhostPartonsCountAcc;
    std::unique_ptr<const SG::AuxElement::Decorator<std::vector<TLorentzVector> > > m_PUJets;

    /// \brief the systematics list we run
  private:
    CP::SysListHandle m_systematicsList{this};

    /// \brief the track collection we run on
  protected:
    CP::SysReadHandle<xAOD::JetContainer> m_jetsHandle{
        this, "jets", "calibrated_jets", "the input jet collection"};

};

#endif // PILEUP_JET_EXTRACTION_ALG_H

