#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

def lumiCalcFiles(sampleType):
    list = []

    if sampleType == '2015' or sampleType == 'MC16a':
        list.append(
            'GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root'
        )

    if sampleType == '2016' or sampleType == 'MC16a':
        list.append(
            'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root'
        )

    if sampleType == '2017' or sampleType == 'MC16d':
        list.append(
            'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root'
        )

    if sampleType == '2018' or sampleType == 'MC16e':
        list.append(
            'GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root'
        )

    if sampleType == 'MC16a':
        assert(len(list) == 2)
    else:
        assert(len(list) == 1)

    return list

def PRWConfigFiles(sampleType):
    list = []

    if sampleType == 'MC16a':
        list.append(
            'PUAnalysisAlgorithms/background_mc16a.root'
        )

    if sampleType == 'MC16d':
        list.append(
            'PUAnalysisAlgorithms/background_mc16d.root'
        )

    if sampleType == 'MC16e':
        list.append(
            'PUAnalysisAlgorithms/background_mc16e.root'
        )

    return list
