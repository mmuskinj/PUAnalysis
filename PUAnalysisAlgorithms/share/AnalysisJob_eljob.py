#!/usr/bin/env python
#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig

import optparse

parser = optparse.OptionParser()
parser.add_option('-i', '--input', dest='input_source',
                  action='store', type='string',
                  help='Input source, either directory or a list of samples')
parser.add_option('-t', '--sample-type', dest='sample_type',
                  action='store', type='string', default='MC16e',
                  help='Type of sample to run over. Valid options are: MC16a, MC16d, MC16e')
parser.add_option('--input-type', dest='input_type',
                  action='store', type='string', default='folder',
                  help='Type of input to run over. Valid options are: folder, xrootd, rucio')
parser.add_option('-d', '--data-type', dest='data_type',
                  action='store', type='string', default='mc',
                  help='Type of data to run over. Valid options are: mc')
parser.add_option('-o', '--submission-dir', dest='submission_dir',
                  action='store', type='string', default='submitDir',
                  help='Submission directory for EventLoop')
parser.add_option('-n', '--nevents', dest='nevents',
                  action='store', type='int', default=100,
                  help='Number of events to process')
parser.add_option('-s', '--skip-events', dest='skip_events',
                  action='store', type='int', default=0,
                  help='Number of events to skip')
parser.add_option('-g', '--grid', dest='grid',
                  action='store', type='string', default='',
                  help='output name for the grid job')
(options, args) = parser.parse_args()

# Store options
inputSource = options.input_source
sampleType = options.sample_type
inputType = options.input_type
dataType = options.data_type

# Validate options
if not inputSource:
    raise Exception('input needs to be set')

if not inputType in ['folder', 'xrootd', 'rucio']:
    raise Exception('invalid input type: ' + inputType)

if not sampleType in ['MC16a', 'MC16d', 'MC16e']:
    raise Exception('invalid sample type: ' + sampleType)

if not dataType in ['mc']:
    raise Exception('invalid data type: ' + dataType)

# Set up (Py)ROOT.
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree', 'CollectionTree')

if inputType == 'rucio':
    raise Exception('rucio input not supported yet')
elif inputType == 'xrootd':
    import os
    # assume format like root://someserver//path/to/files/*pattern*.root
    server, path = inputSource.replace('root://', '').split('//')
    sh_list = ROOT.SH.DiskListXRD(server, os.path.join('/', path), True)
    ROOT.SH.ScanDir().scan(sh, sh_list)
else:
    import os
    sample_base = os.path.basename(inputSource)
    sample_dir = os.path.dirname(os.path.abspath(inputSource))
    mother_dir = os.path.dirname(sample_dir)
    sh_list = ROOT.SH.DiskListLocal(mother_dir)
    ROOT.SH.scanDir(sh, sh_list, sample_base, os.path.basename(sample_dir))

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler(sh)

if options.nevents > 0:
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)

if options.skip_events > 0:
    job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skip_events)

# Set up the systematics loader/handler algorithm:
sysLoader = AnaAlgorithmConfig('CP::SysListLoaderAlg/SysLoaderAlg')
sysLoader.sigmaRecommended = 0
job.algsAdd(sysLoader)

# Include, and then set up the pileup analysis sequence:
from PUAnalysisAlgorithms.Config import lumiCalcFiles, PRWConfigFiles
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
pileupSequence = makePileupAnalysisSequence( dataType,
                                                userLumicalcFiles = lumiCalcFiles(sampleType),
                                                userPileupConfigs = PRWConfigFiles(sampleType) )
pileupSequence.configure( inputName = 'EventInfo',
                          outputName = 'EventInfo_%SYS%' )
# Add all algorithms to the job:
for alg in pileupSequence:
    job.algsAdd( alg )
    pass

# Include, and then set up the jet analysis algorithm sequence:
from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
jetSequence = makeJetAnalysisSequence( dataType,
                                       jetCollection = 'AntiKt4EMTopoJets' )
jetSequence.configure( inputName = 'AntiKt4EMTopoJets',
                       outputName = 'AntiKt4EMTopoJets_calibrated_%SYS%' )
print(jetSequence)
# Add all algorithms to the job:
for alg in jetSequence:
    job.algsAdd( alg )
    pass

# Include, and then set up the jet analysis algorithm sequence:
from PUAnalysisAlgorithms.PUJetExtractionSequence import makePUJetExtractionSequence
jetSeq = makePUJetExtractionSequence(dataType,
                                     deepCopyOutput=True )
jetSeq.configure( inputName = 'AntiKt4EMTopoJets_calibrated_%SYS%',
                  outputName = 'AntiKt4EMTopoJets_PUJets_%SYS%' )
# Add all algorithms to the job:
for alg in jetSeq:
    job.algsAdd(alg)
    pass

# tree mkaer
treeName = 'event'
treeMaker = createAlgorithm('CP::TreeMakerAlg', 'TreeMaker')
treeMaker.TreeName = treeName
job.algsAdd(treeMaker)

# ntuple maker
ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMaker' )
ntupleMaker.TreeName = treeName
ntupleMaker.Branches = [
    'EventInfo.PUJets -> PUJets',
    'EventInfo_NOSYS.runNumber -> runNumber',
    'EventInfo_NOSYS.eventNumber -> eventNumber',
    'EventInfo_NOSYS.RandomRunNumber -> randomRunNumber',
    'EventInfo_%SYS%.PileupWeight -> pileupWeight_%SYS%',
]
ntupleMaker.systematicsRegex = '(^$)'
job.algsAdd(ntupleMaker)

# tree filler
treeFiller = createAlgorithm('CP::TreeFillerAlg', 'TreeFiller')
treeFiller.TreeName = treeName
job.algsAdd(treeFiller)

# Make sure that both the ntuple and the xAOD dumper have a stream to write to.
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Find the right output directory:
submitDir = options.submission_dir

# Run the job using the direct driver.
if options.grid == '':
    driver = ROOT.EL.DirectDriver()
    driver.submit(job, submitDir)
else:
    driver = ROOT.EL.PrunDriver()
    driver.options().setString("nc_outputSampleName", options.grid)
    driver.submitOnly(job, submitDir)
