/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <PUAnalysisAlgorithms/PUJetExtractionAlg.h>

#include <xAODEventInfo/EventInfo.h>

//
// method implementations
//

PUJetExtractionAlg ::PUJetExtractionAlg(const std::string &name,
                                        ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator)
    , m_minJetPt(20000.)
    {
        declareProperty("minJetPt", m_minJetPt);
    }

StatusCode PUJetExtractionAlg ::initialize() {

    // initialize input jets handle
    m_systematicsList.addHandle(m_jetsHandle);
    ANA_CHECK(m_systematicsList.initialize());

    // initialize accessors
    ANA_CHECK(makeSelectionAccessor("jvt_selection", m_jvt_selectionAccessor));
    ANA_CHECK(makeSelectionAccessor("fjvt_selection", m_fjvt_selectionAccessor));

    m_PartonTruthLabelIDAcc = std::make_unique<const SG::AuxElement::Accessor<int> > ("PartonTruthLabelID");
    m_GhostPartonsCountAcc = std::make_unique<const SG::AuxElement::Accessor<int> > ("GhostPartonsCount");
    m_PUJets = std::make_unique<const SG::AuxElement::Decorator< std::vector<TLorentzVector> > > ("PUJets");

    return StatusCode::SUCCESS;
}

StatusCode PUJetExtractionAlg ::execute() {
    return m_systematicsList.foreach (
        [&](const CP::SystematicSet &sys) -> StatusCode {

            // retreive input jets
            const xAOD::JetContainer *jets;
            ANA_CHECK(m_jetsHandle.retrieve(jets, sys));

            // Event info
            const xAOD::EventInfo *eventInfo ;
            ANA_CHECK ( evtStore()->retrieve(eventInfo, "EventInfo") );

            // output jet collection
            std::vector<TLorentzVector> PUJets = {};
                    
            for (const xAOD::Jet *jet : *jets) {
                if (!jet || jet->pt() < m_minJetPt
                         || (*m_PartonTruthLabelIDAcc)(*jet) != -1
                         || !m_jvt_selectionAccessor->getBool(*jet)
                         || !m_fjvt_selectionAccessor->getBool(*jet)
                ) continue;

                // store the jet
                PUJets.push_back(jet->p4());
            }
            
            // save the jet collection in the eventInfo object
            (*m_PUJets)(*eventInfo) = PUJets;

            return StatusCode::SUCCESS;
        });
}
