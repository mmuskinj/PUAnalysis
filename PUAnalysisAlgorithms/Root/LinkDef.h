/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PILEUP_JET_ALGORITHMS_LINKDEF_H
#define PILEUP_JET_ALGORITHMS_LINKDEF_H

#ifdef ROOTCORE
// MN: includes needed by RootCore but posing problems in ROOT 6.6.1. remove protection when ROOT-7879 fixed
// Local include(s):
#include <PUAnalysisAlgorithms/PUJetExtractionAlg.h>
#endif

#ifdef __CINT__

#pragma link C++ nestedclass;

#ifdef XAOD_STANDALONE
#pragma link C++ class vector<TLorentzVector>+;
#endif

#endif // __CINT__
#endif // PILEUP_JET_ALGORITHMS_LINKDEF_H